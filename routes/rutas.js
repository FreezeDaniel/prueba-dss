const express = require('express');
const router = express.Router();
const functions = require('../utils/functions');

router.get('/', functions.mostrarFormulario);
router.post('/enviar', functions.enviarDatos);

module.exports = router;
