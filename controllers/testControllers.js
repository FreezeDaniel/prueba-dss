const functions = require('../utils/functions');

exports.mostrarFormulario = functions.mostrarFormulario;
exports.mostrarDatos = functions.mostrarDatos;
exports.enviarDatos = functions.enviarDatos;
