const sanitizer = require('sanitizer');

exports.mostrarFormulario = (req, res) => {
  res.sendFile('formulario.html', { root: './views' });
};

exports.mostrarDatos = (req, res) => {
  const respuesta = req.query.respuesta;
  res.sendFile('mostrarDatos.html', { root: './views' });
};

exports.sanitizarDato = (dato) => {
  // Sanitizar el dato antes de procesarlo
  const datoSanitizado = sanitizer.sanitize(dato);
  return datoSanitizado;
};

exports.verificarXSS = (dato) => {
  // Verificar si hay inyección XSS en el dato
  // Devuelve true si se detecta una inyección XSS, de lo contrario, devuelve false
  const tieneScript = /<script.*?>.*?<\/script>/i.test(dato);
  return tieneScript;
};

exports.enviarDatos = (req, res) => {
  const datos = req.body;
  const datosSanitizados = this.sanitizarDato(datos.respuesta);
  
  if (this.verificarXSS(datosSanitizados)) {
    // Si se detecta una inyección de código XSS, mostrar página de error
    res.sendFile('error.html', { root: './views' });
  } else {
    // Si no se detecta una inyección de código XSS, redirigir al video de YouTube
    res.redirect('https://www.youtube.com/watch?v=ry53v7vmJpk');
  }
};
