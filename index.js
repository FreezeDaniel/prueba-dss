const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = require('./routes/rutas'); // Importar el enrutador
const PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Montar el enrutador en la ruta '/formulario'
app.use('/formulario', router);

app.listen(PORT, () => {
  console.log(`Cada vez que inicia nodemon 5mil pulgas mueren ${PORT}`);
});
